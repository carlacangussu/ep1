#ifndef PESSOA_HPP
#define PESSOA_HPP

#include <string>

using namespace std;

class Pessoa {
       
	 private:
                string nome;
                string sexo;
                string CPF;
                string endereco;
                int idade;

        public:
                Pessoa();
                ~Pessoa();
                void setNome(string nome);
                string getNome();
                void setSexo(string sexo);
                string getSexo();
                void setCPF(string CPF);
                string getCPF();
                void setEndereco(string endereco);
                string getEndereco();
                void getIdade(int idade);
                int setIdade;
};

#endif

