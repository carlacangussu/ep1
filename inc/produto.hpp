#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <string>

using namespace std;

class Produto {
       
	 private:
		string nome;
		float valor;
		string categoria;
		float classificacao;
		string caracteristicas;
		int quantidade;
	public:
		Produto();
		Produto(string nome, float valor, string categoria, float classificacao, string caracteristicas);
		~Produto();
	
		void setNome(string nome){
			this->nome = nome;
		}
		string getNome(){
			return nome;
		}
		void setValor(float valor){
			this->valor = valor;
		}
		float getValor(){
			return valor;
		}
		void setCategoria(string categoria){
			this->categoria = categoria;
		}
		string getCategoria(){
			return categoria;
		}
		void setClassificacao(float classificacao){
			this->classificacao = classificacao;
		}
		float getClassificacao(){
			return classificacao;
		}
		void setCaracteristicas(string caracteristicas){
			this->caracteristicas = caracteristicas;
		}
		string getCaracteristicas(){
			return caracteristicas;
		}
		void setQuantidade(int quantidade){
			this->quantidade = quantidade;
		}
		int getQuantidae(){
			return quantidade;
		}
		void custo_beneficio();
};
#endif
		
