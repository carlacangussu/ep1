#ifndef SOCIO_HPP
#define SOCIO_HPP

#include <string>
#include "pessoa.hpp"

using namespace std;

class Socio : public Pessoa {
       
	private:
		string hitorico_de_compras;
		

	public:
		Socio();
		~Socio();
		void setHistorico_de_compras(string historico_de_compras){
			this->historico_de_compras = historico_de_compras;
		}
		string getHistorico_de_compras(){
			return historico_de_compras;
		}
		void desconto();
		void recomendacao();
};
#endif 
