#include "pessoa.hpp"
#include <iostream>

Pessoa::Pessoa(){
	nome = "";
	sexo = "";
	CPF = "";
	endereco = "";
	idade = 0;
}
Pessoa::~Pessoa(){}

void Pessoa::setNome(string nome){
	this->nome = nome;
}
string Pessoa::getNome(){
	return nome;
}
void Pessoa::setSexo(string sexo){
	this->sexo = sexo;
}
string Pessoa::getSexo(){
	return sexo;
}
void Pessoa::setCPF(string CPF){
	this->CPF = CPF;
}
string Pessoa::getCPF(){
	return CPF;
}
void Pessoa::setEndereco(string endereco){
	this->endereco = endereco;
}
string Pessoa::getEndereco(){
	return endereco;
}
void Pessoa::setIdade(int idade){
	this->idade = idade;
}
int Pessoa::getIdade(){
	return idade;
}
